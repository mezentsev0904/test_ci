import os
from datetime import datetime

from flask import Flask, request
from flask_restful import Api, Resource  # type: ignore
from flask_sqlalchemy import SQLAlchemy
from marshmallow import ValidationError

path = os.path.join(os.path.abspath(os.path.dirname(__file__)))

db = SQLAlchemy()


def create_app() -> Flask:
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = (
        f'sqlite:///{os.path.join(path, "homework.db")}'
    )
    api = Api(app)

    from .models import Client, ClientParking, Parking

    class ClientListAndCreateApi(Resource):

        def get(self):
            query = db.session.query(Client)
            return [c.to_json() for c in query.all()]

        def post(self):
            from .schemas import CreateClientSchema

            data = request.get_json()
            schema = CreateClientSchema()
            try:
                client = schema.load(data)
                db.session.add(client)
                db.session.commit()
            except ValidationError as e:
                return {"message": e.messages}, 400
            return {"message": "Client created"}, 201

    class ClientByIdApi(Resource):

        def get(self, id):
            client = db.session.query(Client).filter_by(id=id).first()
            if client is None:
                return {"message": "Client not found"}, 404
            return client.to_json()

    class CreateParkingApi(Resource):

        def post(self):
            from .schemas import CreateParkingSchema

            schema = CreateParkingSchema()
            data = request.get_json()
            try:
                parking = schema.load(data)
                db.session.add(parking)
                db.session.commit()
                return {
                    "message": "Parking created",
                }, 201
            except ValidationError as e:
                return {"message": e.messages}, 400

    class ClientParkingsApi(Resource):

        def post(self):
            from .schemas import ClientParkingSchema

            schema = ClientParkingSchema()
            try:
                data = schema.load(request.get_json())
                parking = (
                    db.session.query(Parking).filter_by(id=data["parking_id"]).first()
                )
                if parking is None:
                    return {"message": "Parking not found"}, 404
                elif parking.count_available_places < 1:
                    return {"message": "No places available"}, 404
                elif parking.opened is False:
                    return {"message": "Parking is closed"}
                client = (
                    db.session.query(Client).filter_by(id=data["client_id"]).first()
                )
                if client is None:
                    return {"message": "Client not found"}
                data["time_in"] = datetime.now()
                parking.count_available_places = parking.count_available_places - 1
                db.session.add(
                    ClientParking(
                        client_id=data["client_id"],
                        parking_id=data["parking_id"],
                        time_in=data["time_in"],
                    )
                )
                db.session.commit()
                return {"message": "Welcome to Parking"}, 200
            except ValidationError as e:
                return e.messages, 400

        def delete(self):
            data = request.get_json()
            client_parking = (
                db.session.query(ClientParking)
                .where(
                    ClientParking.client_id == data["client_id"],
                    ClientParking.parking_id == data["parking_id"],
                )
                .first()
            )
            if client_parking:
                parking = (
                    db.session.query(Parking).filter_by(id=data["parking_id"]).first()
                )
                parking.count_available_places = parking.count_available_places + 1
                client = (
                    db.session.query(Client).filter_by(id=data["client_id"]).first()
                )
                if client.credit_card is None:
                    return ({"message": "Please add credit card information"}, 400)
                client_parking.time_out = datetime.now()
                db.session.commit()
                return {"message": "Parking has been paid"}, 200
            return {"message": "Parking rent not found"}, 404

    class OpenParkingApi(Resource):

        def get(self, id):
            parking = db.session.query(Parking).filter_by(id=id).first()
            if parking is None:
                return {"message": "Parking not found"}, 404
            status = request.args.to_dict().get("status")
            if status == "open":
                parking.opened = True
            elif status == "closed":
                parking.opened = False
            elif status is None:
                return {
                    "message": f"Parking id {parking.id} "
                    f"open status - {parking.opened}"
                }, 200
            else:
                return {"message": "Unknown status"}, 400
            db.session.commit()
            return {"parking_opened": parking.opened}, 200

    api.add_resource(ClientByIdApi, "/clients/<int:id>")
    api.add_resource(ClientListAndCreateApi, "/clients")
    api.add_resource(CreateParkingApi, "/parkings")
    api.add_resource(ClientParkingsApi, "/client_parkings")
    api.add_resource(OpenParkingApi, "/parkings/<int:id>")

    return app
