from marshmallow import Schema, ValidationError, fields, post_load

from .models import Client, Parking


class CreateClientSchema(Schema):
    name = fields.Str(required=True)
    surname = fields.Str(required=True)
    credit_card = fields.Str()
    car_number = fields.Str()

    @post_load
    def create_client(self, data, **kwargs):
        return Client(**data)


class CreateParkingSchema(Schema):
    address = fields.Str(required=True)
    opened = fields.Bool(dump_default=False)
    count_places = fields.Int(required=True)
    count_available_places = fields.Int(required=True)

    @post_load
    def create_parking(self, data, **kwargs):
        parking = Parking(**data)
        print(parking.opened)
        if parking.count_available_places > parking.count_places:
            raise ValidationError(
                "Количество доступных мест не может " "превышать общее количество мест."
            )
        return Parking(**data)


class ClientParkingSchema(Schema):
    client_id = fields.Int(required=True)
    parking_id = fields.Int(required=True)

    @post_load
    def ids(self, data, **kwargs):
        return {"client_id": data["client_id"], "parking_id": data["parking_id"]}
