import datetime
import os

import pytest

from ..app.app_factory import create_app
from ..app.app_factory import db as _db
from ..app.app_factory import path


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = (
        f'sqlite:///{os.path.join(path, "test.db")}'
    )

    _db.init_app(_app)

    with _app.app_context():
        from ..app.models import Client, ClientParking, Parking

        _db.create_all()
        client_1 = Client(
            name="Sam",
            surname="Likov",
            credit_card="1523-1234-5678-8571",
            car_number="L283KA",
        )
        client_2 = Client(
            name="Peter",
            surname="Trukov",
            credit_card="5721-9581-3216-7214",
            car_number="T981LA",
        )
        parking = Parking(
            address="62 Letnaya, Novosibirsk",
            count_places=900,
            count_available_places=900,
            opened=True,
        )
        _db.session.add(client_1)
        _db.session.add(client_2)
        _db.session.add(parking)
        client_parking = ClientParking(
            client_id=2, parking_id=1, time_in=datetime.datetime.now()
        )
        _db.session.add(client_parking)
        _db.session.commit()
        yield _app
        _db.session.close()
        _db.drop_all()
        os.remove(f"{path}/test.db")


@pytest.fixture
def client(app):
    yield app.test_client()


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
