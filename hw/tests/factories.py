import random

import factory  # type: ignore

from ..app.models import Client, Parking
from .conftest import _db


class ClientFactory(factory.alchemy.SQLAlchemyModelFactory):

    class Meta:
        model = Client
        sqlalchemy_session = _db.session

    name = factory.Faker("first_name", locale="ru_RU")
    surname = factory.Faker("last_name", locale="ru_RU")
    credit_card = factory.Faker("credit_card_number")
    car_number = factory.Faker("license_plate")


class ParkingFactory(factory.alchemy.SQLAlchemyModelFactory):

    class Meta:
        model = Parking
        sqlalchemy_session = _db.session

    address = factory.Faker("address")
    opened = factory.Faker("random_element", elements=(True, False))
    count_places = factory.Faker("random_int", min=100, max=1000)

    @factory.lazy_attribute
    def count_available_places(self):
        return random.randint(0, self.count_places)
