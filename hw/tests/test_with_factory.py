import pytest

from ..app.models import Client, Parking
from .factories import ClientFactory, ParkingFactory, _db


@pytest.mark.factories
def test_client_create_with_factory(client):
    c = ClientFactory()
    _db.session.commit()
    assert c.id is not None
    response = client.get(f"/clients/{c.id}")
    assert response.status_code == 200
    assert _db.session.query(Client).count() == 3


@pytest.mark.factories
def test_parking_create_with_factory(client):
    p = ParkingFactory()
    _db.session.commit()
    assert p.id is not None
    response = client.get(f"/parkings/{p.id}")
    assert response.status_code == 200
    assert response.json in [
        {"message": "Parking id 2 open status - True"},
        {"message": "Parking id 2 open status - False"},
    ]
    assert _db.session.query(Parking).count() == 2
