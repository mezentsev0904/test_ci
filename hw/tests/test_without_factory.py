import pytest


def test_client_by_id(client):
    response = client.get("/clients/1")
    assert response.status_code == 200
    assert response.json == {
        "id": 1,
        "name": "Sam",
        "surname": "Likov",
        "credit_card": "1523-1234-5678-8571",
        "car_number": "L283KA",
    }


@pytest.mark.parametrize(
    "url, status",
    [
        ("http://localhost:/clients", 200),
        ("http://localhost:/clients/1", 200),
        ("http://localhost:/clients/3", 404),
    ],
)
def test_all_get_endpoints(client, url, status):
    response = client.get(url)
    assert response.status_code == status


def test_client_creation(client):
    data = {
        "name": "Sergey",
        "surname": "Plush",
        "car_number": "I872UJ",
        "credit_card": "3333-0000-1234-8585",
    }
    response = client.post("http://localhost/clients", json=data)
    assert response.status_code == 201
    assert response.json == {"message": "Client created"}


def test_parking_creation_and_check_open_status(client):
    data = {
        "address": "98 Tupolevskaya, Omsk",
        "count_places": 300,
        "count_available_places": 300,
    }
    response = client.post("http://localhost/parkings", json=data)
    assert response.status_code == 201
    assert response.json == {"message": "Parking created"}


@pytest.mark.parking
def test_parking_open_status(client):
    response = client.get("http://localhost/parkings/1")
    assert response.status_code == 200
    assert response.json == {"message": "Parking id 1 open status - True"}


@pytest.mark.parking
def test_parking_check_in(client):
    data = {"client_id": 1, "parking_id": 1}
    response = client.post("http://localhost/client_parkings", json=data)
    assert response.status_code == 200
    assert response.json == {"message": "Welcome to Parking"}


@pytest.mark.parking
def test_parking_check_out(client):
    data = {"client_id": 2, "parking_id": 1}
    response = client.delete("http://localhost/client_parkings", json=data)
    assert response.status_code == 200
    assert response.json == {"message": "Parking has been paid"}
